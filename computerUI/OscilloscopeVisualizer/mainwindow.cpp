#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QtSerialPort/QSerialPort>
#include <QtCharts>
#include <QWidget>

#include <iostream>
#include <mutex>
#include <atomic>

#include <QtCharts/QChartView>
#include <QtCharts/QLineSeries>
#include <QtCharts/QChart>
#include <QtCharts/QValueAxis>

#define defaultPortName "ttyUSB0"


/**
*   Window constructor
*/
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , killSignal(false)
{
        ui->setupUi(this);
}

void MainWindow::initializeContent()
{
    //Allocate space for the QLineSerie object
    this->buffer = new QLineSeries;
    //initialize the buffer with values
    float x = 0;
    for(int i = 0; i <= 100; i++, x++)
    {
        this->buffer->insert(i, QPointF(x, 0.0));
    }

    //Create the chart object
    this->graph = new QChart;

    //Create the graph object
    this->chartView = new QChartView(graph);
    chartView->setMinimumSize(800, 600);
    graph->addSeries(this->buffer);

    //Adjust format and shown values of the axis
    this->axisX = new QValueAxis;
    axisX->setRange(0, 100);
    axisX->setLabelFormat("%g");
    axisX->setTitleText("time");

    //same goes for the 2nd axis of the graph
    this->axisY = new QValueAxis;
    axisY->setRange(-1, 1);
    axisY->setTitleText("Voltage (V)");

    graph->addAxis(axisX, Qt::AlignBottom);
    this->buffer->attachAxis(axisX);
    graph->addAxis(axisY, Qt::AlignLeft);
    this->buffer->attachAxis(axisY);
    graph->legend()->hide();

    //Use the chart view as the main widget of the main window
    //(if this were a normal widget it would be layout->addWidget())

    this->containerWidget = new QWidget(this);
    this->mainLayout = new QVBoxLayout();
    this->ttyLabel = new QLabel("Arduino port");
    this->ttyEdit = new QLineEdit();
    this->ttyEdit->setText("/dev/ttyACM0");

    QString stringBuf;
    connect(ttyEdit, &QLineEdit::returnPressed,
            this,    &MainWindow::changeSerialPortName);

    this->containerWidget = new QWidget(this);
    this->mainLayout = new QVBoxLayout();

    mainLayout->addWidget(this->ttyLabel);
    mainLayout->addWidget(this->ttyEdit);
    mainLayout->addWidget(chartView);

    containerWidget->setLayout(mainLayout);
    this->setCentralWidget(containerWidget);

    //Show what was added
    this->update();

    QString name = QString(defaultPortName);
    this->serialPort = new QSerialPort(name, nullptr);

    //start main loop
    this->threadHandle = new std::thread(&MainWindow::readingLoop, this);
}

/**
*   action loop
*/

void MainWindow::readingLoop()
{
    this->testDisplay();
    try {
        while(this->killSignal != true){
            if (this->serialPort->isOpen() == true)
            {
                if(this->serialPort->bytesAvailable() > 0)
                {
                    std::mutex m1;
                    std::lock_guard<std::mutex> lock(m1);
                    char buff[256];
                    this->serialPort->read(buff, 255);
                    this->serialPort->flush();
                }
            }
        }
    } catch (std::exception * e) {
        std::cout << e->what() << std::endl;
    }
}

/**
*   Window destructor
*/
MainWindow::~MainWindow()
{
    //tell the thread to stop its loop
    this->killSignal = true;
    //join the thread waiting for it to stop gracefully
    this->threadHandle->join();

    //delete allocated class elements
    delete ui;

    delete this->serialPort;
    delete this->buffer;

    //delete the graph elements
    delete this->graph;
    delete this->chartView;

    delete this->containerWidget;
}


void MainWindow::changeSerialPortName()
{
        try {
            //kill worker thread
            this->killSignal = true;
            //wait for thread to finish its execution
            this->threadHandle->join();
            //close the serial port that was initialised with obsolete parameters
            this->serialPort->close();
            //reinitialise the serial port with new name
            this->serialPort->setPortName(this->ttyEdit->text());
            bool ret = this->serialPort->open(QIODevice::ReadWrite);

            //if something failed
            if(ret == false)
            {
                //print the error to standard output
                std::cout << this->serialPort->errorString().toStdString() << std::endl;
            }
            else
            {
                //print a friendly message
                std::cout << "Port is set to " << this->ttyEdit->text().toStdString() << std::endl;
            }
        } catch (std::exception * e) {
            std::cout << e->what() << std::endl;
        }
        this->threadHandle = new std::thread(&MainWindow::readingLoop, this);
}

void MainWindow::testDisplay()
{
    try {
        for(int i = 0; i < 100; i++)
        {
            this->buffer->replace(i, QPointF(i, cos(i * 0.1)));
        }
        this->chartView->update();
    } catch (std::exception * e) {
        std::cout << e->what() << std::endl;
    }
}
