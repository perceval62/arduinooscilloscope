#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <mutex>
#include <atomic>

#include <QMainWindow>

#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>

#include <QWidget>
#include <QLineEdit>
#include <QLabel>
#include <QVBoxLayout>
#include <iostream>

#include <QtCharts/QChartView>
#include <QtCharts/QLineSeries>
#include <QtCharts/QChart>
#include <QtCharts/QValueAxis>

#include <thread>

#include <QLineSeries>

using namespace QtCharts;
QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:

    /**
    * @brief MainWindow QWidget object that is used as the main window of the project.
    * @param parent QObject that has ownership over the mainwindow.
    */
    MainWindow(QWidget *parent = nullptr);
    void readingLoop();
    void initializeContent();

    ~MainWindow();
public slots:
    void changeSerialPortName();
private:
    void testDisplay();

    Ui::MainWindow *ui;
    //serial port handle
    QSerialPort * serialPort;

    //handles for graph elements
    QLineSeries * buffer;
    QChart * graph;
    QChartView * chartView;
    QValueAxis *axisX;
    QValueAxis *axisY;

    QWidget * containerWidget;
    QVBoxLayout * mainLayout;
    QLabel * ttyLabel;
    QLineEdit * ttyEdit;

    volatile bool killSignal;
    std::thread * threadHandle;


};
#endif // MAINWINDOW_H
