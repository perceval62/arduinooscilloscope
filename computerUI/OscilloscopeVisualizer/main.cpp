#include "mainwindow.h"

#include <QApplication>

#include <thread>
#include <iostream>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;

    w.initializeContent();
    w.show();


    return a.exec();

}
