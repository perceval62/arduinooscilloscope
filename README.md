# Oscilloscope software

This software is made with the Qt framework and will enable anyone<br>
to analyze analog signals with any micro controller with USB capabilities<br> 
and ADC (analog digital converter)<hr>

## Future features:
-Zoom on the waveform.<br>
-Multiple channels.<br>
-View.<br>
-Input from multiple waveforms.<br>

## Why ?

Even though a micro controller's ADC has nowhere near the precision of normal oscilloscopes,<br>
it can be useful to have a way to quickly modelise an analog waveform.<br>
